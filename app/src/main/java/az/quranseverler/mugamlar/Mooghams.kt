package az.quranseverler.mugamlar

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import az.quranseverler.maqamat.R
import kotlinx.android.synthetic.main.activity_mooghams.*

class Mooghams : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mooghams)
    }
    public fun listenOnClick(view: View){
        when(view){
            bayat_view -> {
                val intent = Intent(this@Mooghams, InternalMoogham::class.java)
                intent.putExtra("MooghamName", "bayat")
                intent.putExtra("Moogham", bayat_view.text.toString())
                intent.putExtra("MasterName", "Mustafa İsmail")
                intent.putExtra("moogham", false)
                startActivity(intent)
            }
            nahavand_view -> {
                val intent = Intent(this@Mooghams, InternalMoogham::class.java)
                intent.putExtra("MooghamName", "nahavand")
                intent.putExtra("Moogham", nahavand_view.text.toString())
                intent.putExtra("MasterName", "Mustafa İsmail")
                intent.putExtra("moogham", false)
                startActivity(intent)
            }
            saba_view -> {
                val intent = Intent(this@Mooghams, InternalMoogham::class.java)
                intent.putExtra("MooghamName", "saba")
                intent.putExtra("Moogham", saba_view.text.toString())
                intent.putExtra("MasterName", "Minşavi")
                intent.putExtra("moogham", false)
                startActivity(intent)}
            rast_view -> {
                val intent = Intent(this@Mooghams, InternalMoogham::class.java)
                intent.putExtra("MooghamName", "rast")
                intent.putExtra("Moogham", rast_view.text.toString())
                intent.putExtra("MasterName", "Əbdülbasit")
                intent.putExtra("moogham", false)
                startActivity(intent)}
            hicaz_view -> {
                val intent = Intent(this@Mooghams, InternalMoogham::class.java)
                intent.putExtra("MooghamName", "hicaz")
                intent.putExtra("Moogham", hicaz_view.text.toString())
                intent.putExtra("MasterName", "Minşavi")
                intent.putExtra("moogham", false)
                startActivity(intent)}
            cahargah_view -> {
                val intent = Intent(this@Mooghams, InternalMoogham::class.java)
                intent.putExtra("MooghamName", "cahar")
                intent.putExtra("Moogham", cahargah_view.text.toString())
                intent.putExtra("MasterName", "Əbdülbasit")
                intent.putExtra("moogham", false)
                startActivity(intent)}
            ecem_view -> {
                val intent = Intent(this@Mooghams, InternalMoogham::class.java)
                intent.putExtra("MooghamName", "ecem")
                intent.putExtra("Moogham", ecem_view.text.toString())
                intent.putExtra("MasterName", "Mustafa İsmail")
                intent.putExtra("moogham", false)
                startActivity(intent)}
            segah_view -> {
                val intent = Intent(this@Mooghams, InternalMoogham::class.java)
                intent.putExtra("MooghamName", "segah")
                intent.putExtra("Moogham", segah_view.text.toString())
                intent.putExtra("MasterName", "Minşavi")
                intent.putExtra("moogham", false)
                startActivity(intent)}

        }
    }
}
