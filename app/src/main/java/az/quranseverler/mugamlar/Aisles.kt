package az.quranseverler.mugamlar

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import az.quranseverler.maqamat.R
import kotlinx.android.synthetic.main.activity_aisles.*

class Aisles : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_aisles)
    }
    public fun listenOnClick(view: View){
        when(view){
            rastdan_segaha_view -> {
                val intent = Intent(this@Aisles, PlayMoogham::class.java)
                intent.putExtra("mp3Name", "rastdan_segaha")
                intent.putExtra("mooghamItself", "Rastdan Segaha keçid")
                intent.putExtra("moogham", true)
                startActivity(intent)
            }
            sabadan_hicaza_view -> {
                val intent = Intent(this@Aisles, PlayMoogham::class.java)
                intent.putExtra("mooghamItself", "Səbadan Hicaza keçid")
                intent.putExtra("mp3Name", "sabadan_hicaza")
                intent.putExtra("moogham", true)
                startActivity(intent)
            }
            segahdan_rasta_view -> {
                val intent = Intent(this@Aisles, PlayMoogham::class.java)
                intent.putExtra("mooghamItself", "Segahdan Rasta keçid")
                intent.putExtra("mp3Name", "segahdan_rasta")
                intent.putExtra("moogham", true)
                startActivity(intent)}
            bayatdan_rasta_view -> {
                val intent = Intent(this@Aisles, PlayMoogham::class.java)
                intent.putExtra("mooghamItself", "Bəyatdan Rasta keçid")
                intent.putExtra("mp3Name", "bayatdan_rasta")
                intent.putExtra("moogham", true)
                startActivity(intent)}
            bayatdan_sabaya_view -> {
                val intent = Intent(this@Aisles, PlayMoogham::class.java)
                intent.putExtra("mooghamItself", "Bəyatdan Səbaya keçid")
                intent.putExtra("mp3Name", "bayatdan_sabaya")
                intent.putExtra("moogham", true)
                startActivity(intent)}
            hicazdan_nahavanda_view -> {
                val intent = Intent(this@Aisles, PlayMoogham::class.java)
                intent.putExtra("mooghamItself", "Hicazdan Nəhavəndə  keçid")
                intent.putExtra("mp3Name", "hicazdan_nahavanda")
                intent.putExtra("moogham", true)
                startActivity(intent)}
            nahavanddan_rasta -> {
                val intent = Intent(this@Aisles, PlayMoogham::class.java)
                intent.putExtra("mooghamItself", "Nəhavənddən Rasta keçid")
                intent.putExtra("mp3Name", "nahavanddan_rasta")
                intent.putExtra("moogham", true)
                startActivity(intent)}

        }
    }
}
