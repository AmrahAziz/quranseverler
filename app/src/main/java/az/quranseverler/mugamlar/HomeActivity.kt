package az.quranseverler.mugamlar

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import az.quranseverler.maqamat.R
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setListeners()
        //var mediaPlayer: MediaPlayer? = MediaPlayer.create(this, R.raw.bayatdan_rasta)

            //startActivity()
               // mediaPlayer?.start()
    }

    private fun setListeners() {
//        bayat_view.setOnClickListener{
//            val intent = Intent(this@HomeActivity, InternalMoogham::class.java)
//            intent.putExtra("MooghamName", "bayat")
//            intent.putExtra("moogham", false)
//            startActivity(intent)
//        }
        bayatdan_rasta_view.setOnClickListener{

            val intent = Intent(this@HomeActivity, PlayMoogham::class.java)
            intent.putExtra("mp3Name", "bayatdan_rasta")
            intent.putExtra("moogham", true)
            startActivity(intent)
        }
        rastdan_segaha_view.setOnClickListener{
            val intent = Intent(this@HomeActivity, PlayMoogham::class.java)
            intent.putExtra("mp3Name", "rastdan_segaha")
            intent.putExtra("moogham", true)
            startActivity(intent)
        }
        sabadan_hicaza_view.setOnClickListener{
            val intent = Intent(this@HomeActivity, PlayMoogham::class.java)
            intent.putExtra("mp3Name", "sabadan_hicaza")
            intent.putExtra("moogham", true)
            startActivity(intent)
        }
        segahdan_rasta_view.setOnClickListener{
            val intent = Intent(this@HomeActivity, PlayMoogham::class.java)
            intent.putExtra("mp3Name", "segahdan_rasta")
            intent.putExtra("moogham", true)
            startActivity(intent)
        }
//        saba_view.setOnClickListener{
//
//            val intent = Intent(this@HomeActivity, InternalMoogham::class.java)
//            intent.putExtra("MooghamName", "saba")
//            intent.putExtra("moogham", false)
//            startActivity(intent)
//        }
//        rast_view.setOnClickListener{
//
//            val intent = Intent(this@HomeActivity, InternalMoogham::class.java)
//            intent.putExtra("MooghamName", "rast")
//            intent.putExtra("moogham", false)
//            startActivity(intent)
//        }
//        nahavand_view.setOnClickListener{
//            val intent = Intent(this@HomeActivity, InternalMoogham::class.java)
//            intent.putExtra("MooghamName", "nahavand")
//            intent.putExtra("moogham", false)
//            startActivity(intent)}
//        hicaz_view.setOnClickListener{
//            val intent = Intent(this@HomeActivity, InternalMoogham::class.java)
//            intent.putExtra("MooghamName", "hicaz")
//            intent.putExtra("moogham", false)
//            startActivity(intent)}
//        cahargah_view.setOnClickListener{
//            val intent = Intent(this@HomeActivity, InternalMoogham::class.java)
//            intent.putExtra("MooghamName", "cahar")
//            intent.putExtra("moogham", false)
//            startActivity(intent)}
//        segah_view.setOnClickListener{
//            val intent = Intent(this@HomeActivity, InternalMoogham::class.java)
//            intent.putExtra("MooghamName", "segah")
//            intent.putExtra("moogham", false)
//            startActivity(intent)}
//        ecem_view.setOnClickListener{
//            val intent = Intent(this@HomeActivity, InternalMoogham::class.java)
//            intent.putExtra("MooghamName", "ecem")
//            intent.putExtra("moogham", false)
//            startActivity(intent)}
        bayatdan_sabaya_view.setOnClickListener{
            val intent = Intent(this@HomeActivity, PlayMoogham::class.java)
            intent.putExtra("mp3Name", "bayatdan_sabaya")
            intent.putExtra("moogham", true)
            startActivity(intent)
        }
        hicazdan_nahavanda_view.setOnClickListener{
            val intent = Intent(this@HomeActivity, PlayMoogham::class.java)
            intent.putExtra("mp3Name", "hicazdan_nahavanda")
            intent.putExtra("moogham", true)
            startActivity(intent)
        }
        nahavanddan_rasta.setOnClickListener{
            val intent = Intent(this@HomeActivity, PlayMoogham::class.java)
            intent.putExtra("mp3Name", "nahavanddan_rasta")
            intent.putExtra("moogham", true)
            startActivity(intent)
        }
    }
}
