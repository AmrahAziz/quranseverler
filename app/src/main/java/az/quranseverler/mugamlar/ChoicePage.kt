package az.quranseverler.mugamlar

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Toast
import az.quranseverler.maqamat.R
import kotlinx.android.synthetic.main.activity_choice_page.*

class ChoicePage : AppCompatActivity() {
    private var doubleBackToExitPressedOnce = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choice_page)
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            finishAffinity()
            return
        }

        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Çıxmaq üçün bir daha klikləyin", Toast.LENGTH_SHORT).show()

        Handler().postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 2000)
    }

    public fun listenOnClick(view: View) {
        if(view==kecidler){
            startActivity(Intent(this, Aisles::class.java))
        }else if(view==mugamlar){

            startActivity(Intent(this, Mooghams::class.java))
        }
    }
}
