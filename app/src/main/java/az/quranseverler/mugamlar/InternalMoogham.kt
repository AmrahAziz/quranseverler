package az.quranseverler.mugamlar

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import az.quranseverler.maqamat.R
import kotlinx.android.synthetic.main.activity_internal_moogham.*

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS", "RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class InternalMoogham : AppCompatActivity() {
    var mooghamName: String = ""
    var moogham: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_internal_moogham)

        val bundle: Bundle? = intent.extras
        mooghamName = bundle?.getString("MooghamName")?: ""
        moogham = bundle?.getString("Moogham")?: ""
        checkExampleVisibility()
        numune_hisse_view.text = bundle?.getString("MasterName")


        val intent = Intent(this, PlayMoogham::class.java)
        intent.putExtra("moogham", false)
        qerar_hisse_view.setOnClickListener{
            intent.putExtra("mp3Name", mooghamName +"_1")
            intent.putExtra("mooghamItself", "${moogham} Qərar hissəsi")
            startActivity(intent)
        }
        cavab_hisse_view.setOnClickListener{
            intent.putExtra("mp3Name", mooghamName+"_2")
            intent.putExtra("mooghamItself", "${moogham} Cavab hissəsi")
            startActivity(intent)
        }
        cavabi_cavab_hisse_view.setOnClickListener{
            intent.putExtra("mp3Name", mooghamName+"_3")
            intent.putExtra("mooghamItself", "${moogham} Cəvabi-Cavab hissəsi")
            startActivity(intent)
        }
        tevase_hisse_view.setOnClickListener{

            intent.putExtra("mp3Name", mooghamName+"_5")
            intent.putExtra("mooghamItself", "${moogham} Təvaşi üstündə")
            startActivity(intent)
        }
        numune_hisse_view.setOnClickListener{
            intent.putExtra("mp3Name", mooghamName+"_4")
            intent.putExtra("mooghamItself", "${numune_hisse_view.text.toString()} ifasında $moogham")
            startActivity(intent)
        }

    }


    fun checkExampleVisibility(){
        if(mooghamName.equals("nahavand"))
            tevase_hisse_view.visibility= View.GONE
    }

}
