package az.quranseverler.mugamlar

import android.content.Context
import az.quranseverler.maqamat.R

class MooghamUtils {
    val MAX_PROGRESS: Int = 1000
    fun milliSecondsToTime(milliseconds: Long): String{
        var finalTimerString: String = ""
        var secondsString: String = ""
        val hours = (milliseconds/(1000*60*60)).toInt()

        val minutes = (milliseconds%(1000*60*60))/(1000*60).toInt()

        val seconds = ((milliseconds/(1000*60*60))%(1000*60))/1000.toInt()
        if(hours>0){
            finalTimerString = "$hours:"
        }
        if(seconds<10){
            secondsString="0$seconds"
        }else{
            secondsString="$seconds"
        }
        finalTimerString = "$finalTimerString$minutes:$secondsString"
        return finalTimerString
    }
    fun getProgressSeekBar(currentDuration: Long, totalDuration: Int): Int{
        var progress: Double = 0.00
            progress = ((currentDuration.toDouble())/totalDuration)*MAX_PROGRESS

        return progress.toInt()

    }
    fun progressToTimer(progress: Int, _totalDuration: Int): Int{
        var currentDuration: Int =0
        val totalDuration: Int = _totalDuration/1000

        currentDuration = ((progress/MAX_PROGRESS).toDouble()*totalDuration).toInt()

        return currentDuration*1000

    }

}

fun String.getVerseByMooghamName(context: Context) =
    when (this) {
        "rastdan_segaha" -> context.getString(R.string.rastdan_segaha)
        "sabadan_hicaza" -> context.getString(R.string.sabadan_hicaza)
        "segahdan_rasta" -> context.getString(R.string.segahdan_rasta)
        "bayatdan_rasta" -> context.getString(R.string.bayatdan_rasta)
        "bayatdan_sabaya" -> context.getString(R.string.bayatdan_sabaya)
        "hicazdan_nahavanda" -> context.getString(R.string.hicazdan_nahavanda)
        "nahavanddan_rasta" -> context.getString(R.string.nahavanddan_rasta)
        "bayat_1" -> context.getString(R.string.bayat_1)
        "bayat_2" -> context.getString(R.string.bayat_2)
        "bayat_3" -> context.getString(R.string.bayat_3)
        "bayat_4" -> context.getString(R.string.bayat_4)
        "bayat_5" -> ""
        "rast_1" -> context.getString(R.string.rast_1)
        "rast_2" -> context.getString(R.string.rast_2)
        "rast_3" -> context.getString(R.string.rast_3)
        "rast_4" -> context.getString(R.string.rast_4)
        "rast_5" -> ""
        "saba_1" -> context.getString(R.string.saba_1)
        "saba_2" -> context.getString(R.string.saba_2)
        "saba_3" -> context.getString(R.string.saba_3)
        "saba_4" -> context.getString(R.string.saba_4)
        "saba_5" -> ""
        "segah_1" -> context.getString(R.string.segah_1)
        "segah_2" -> context.getString(R.string.segah_2)
        "segah_3" -> context.getString(R.string.segah_3)
        "segah_4" -> context.getString(R.string.segah_4)
        "segah_5" -> ""
        "ecem_1" -> context.getString(R.string.ecem_1)
        "ecem_2" -> context.getString(R.string.ecem_2)
        "ecem_3" -> context.getString(R.string.ecem_3)
        "ecem_4" -> context.getString(R.string.ecem_4)
        "ecem_5" -> ""
        "hicaz_1" -> context.getString(R.string.hicaz_1)
        "hicaz_2" -> context.getString(R.string.hicaz_2)
        "hicaz_3" -> context.getString(R.string.hicaz_3)
        "hicaz_4" -> context.getString(R.string.hicaz_4)
        "hicaz_5" -> ""
        "cahar_1" -> context.getString(R.string.cahar_1)
        "cahar_2" -> context.getString(R.string.cahar_2)
        "cahar_3" -> ""
        "cahar_4" -> context.getString(R.string.cahar_4)
        "cahar_5" -> ""
        "nahavand_1" -> context.getString(R.string.nahavand_1)
        "nahavand_2" -> context.getString(R.string.nahavand_2)
        "nahavand_3" -> context.getString(R.string.nahavand_3)
        "nahavand_4" -> context.getString(R.string.nahavand_4)
        else -> ""
    }
